is_ios = /iPhone|iPad|iPod/.test ua = navigator.userAgent
user_id = /HeyBeautyUserId\/(\d+)/.exec(ua)?[1]

section = $('section')
detail = $('.detail')
list = $('.list')
ul = $('<ul></ul>').prependTo list

count = 0
items = []
loading = undefined

getJson = ->
  loading = true
  $.get "http://assets.heybeauty.me/kr/app/data/notice_#{count}.json?date=#{Date.now()}"
  .then (res) ->
    if count++ is res.last_page then finish()
    items = items.concat res.data
    _.each res.data, (item) ->
      li = $('<li></li>').append a = $('<a></a>')
      $('<span></span>').text(item.title).appendTo a
      $('<span></span>').text(item.published_at.replace /(\d{4})(\d{2})(\d{2})/, '$1/$2/$3').appendTo a
      a.on 'click', -> open item.id, item.body_html
      li.appendTo ul
    loading = false
    init()

init = ->
  params = /(open|id)=(\d+)/i.exec location.hash
  if item = _.find(items, id: Number params?[2])
    if params[1] is 'id'
      draw item.body_html
    else
      history.replaceState {}, '', '#'
      open item.id, item.html
  else
    close()

scrollable = -> ul.outerHeight() - list.scrollTop() > list.height() * 1.3
interval = setInterval ->
  return if scrollable() or loading
  getJson()
, 450

finish = ->
  $('.indicator').remove()
  clearInterval interval

open = (id, html) ->
  history.pushState {id: id}, '', "#id=#{id}"
  draw html

draw = (html) ->
  detail.empty().html html
  section.addClass 'open'

close = ->
  section.removeClass 'open'

$(window).on 'hashchange', init