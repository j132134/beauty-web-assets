indents = ['0%', '-100%', '-200%', '-300%']
current = 0

setInterval ->
  $('.images').animate { textIndent: indents[current++] }, 350
  if current == 4 then current = 0
, 2200

data = {}
$('button').on 'click', ->
  data = do ->
    o = {}
    $.each $('#join').serializeArray(), ->
      if !@value
        return
      if ! !o[@name]
        o[@name] += ',' + @value
      else
        o[@name] = @value
      return
    o.params = location.search.slice(1)
    return o

  if !data.shop_name
    notify 'warn', '업체명을 입력해주세요.'
  else if !data.address
    notify 'warn', '주소를 입력해주세요.'
  else if !data.category
    notify 'warn', '업종을 선택해주세요.'
  else if !data.owner_name
    notify 'warn', '신청자 이름을 입력해주세요.'
  else if !data.phone_num
    notify 'warn', '연락가능한 전화번호를 입력해주세요.'
  else if !data.contact_day
    notify 'warn', '통화 가능 요일을 1개 이상 선택해주세요.'
  else if !data.contact_time
    notify 'warn', '통화 희망 시간을 입력해주세요.'
  else if !data.want
    notify 'warn', '헤이뷰티에 바라는 점을 선택해주세요.'
  else
    submit data

box = $('.notify')
notify = (type, message, fadeOut = true) ->
  box.removeClass().addClass("notify #{type}").text message
  box.fadeIn 250
  setTimeout (-> box.fadeOut 750), 2500 if fadeOut

wait = null
submit = (data) ->
  if wait and _.isEqual wait, data
    notify 'saved', '이미 신청 완료 되었습니다. 감사합니다.'
    return

  wait = data
  notify 'wait', '잠시만 기다려주세요..', false
  $.post 'https://gobeauty.slack.com/services/hooks/slackbot?token=vn3bFTItpcsLZ6hTZiALfdP7&channel=%23operation',
    "
    헤이뷰티 가입 상담 신청이 들어왔습니다.\n
    - 샵 이름: #{data.shop_name}\n
    - 주소: #{data.address}\n
    - 카테고리: #{data.category}\n
    - 이름/직함: #{data.owner_name}\n
    - 연락처: #{data.phone_num}\n
    - 통화가능요일: #{data.contact_day}\n
    - 통화가능시간: #{data.contact_time}\n
    - 원하는점: #{data.want}\n
    - 추적코드: #{data.params}\n
    "
  .done ->
    $.post 'https://script.google.com/macros/s/AKfycbwc4jNbJ9u0fZ6ZG8dcl8qzlOO9zkL_BVB3KlVOdFoi5fxftRza/exec', data
    notify 'success', '신청 완료 되었습니다. 감사합니다.'
  .fail ->
    notify 'error', '전송이 실패했습니다. 다시 시도해주세요.'
    wait = null
