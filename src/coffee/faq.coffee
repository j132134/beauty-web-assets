is_ios = /iPhone|iPad|iPod/.test ua = navigator.userAgent
user_id = /HeyBeautyUserId\/(\d+)/.exec(ua)?[1]

section = $('section')
detail = $('.detail').children 'article'
list = $('.list')
ul = $('<ul></ul>').prependTo list

items = []
$.get "http://assets.heybeauty.me/kr/app/data/faq.json?date=#{Date.now()}"
.then (res) ->
  items = res.data
  _.each items, (item, index) ->
    li = $('<li></li>').append a = $('<a></a>')
    $('<span></span>').text(item.section).appendTo a
    $('<span></span>').text(item.question).appendTo a
    a.on 'click', -> open index, item
    li.appendTo ul
  init()

init = ->
  params = /q=(\d+)/i.exec location.hash
  if index = params?[1]
    draw items[index]
  else
    close()

open = (index, item) ->
  history.pushState {q: index}, '', "#q=#{index}"
  draw item

draw = (item) ->
  detail.empty()
  $('<div></div>').addClass('answer').text(item.answer).prependTo detail
  question = $('<div></div>').addClass('question').prependTo detail
  $('<span></span>').text(item.section).appendTo question
  $('<span></span>').text(item.question).appendTo question
  section.addClass 'open'

close = ->
  section.removeClass 'open'

$(window).on 'hashchange', init