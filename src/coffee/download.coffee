utm = {}

params = location.search.substring(1).split('&')
utm[(result = param.split('='))[0]] = result[1] for param in params

ios_campaign_id = utm.source + '%2B' + utm.campaign
console.log link = utm.link and decodeURIComponent utm.link

if utm.medium
  _array = utm.medium.split '_'
  platform = _array[0]
  app = _array[1]

  # detect browser
  if platform == 'detect'
    agent = navigator.userAgent
    ios = /(iPhone|iPad|iPod).*OS ([0-9_]+)/i.test agent
    android = /Android ([0-9.]+)/i.test agent

    platform = if ios then 'ios' else if android then 'android' else 'unknown'

  # generate url
  if platform == 'android'
    if app == 'client'
      url = 'https://play.google.com/store/apps/details?id=me.heybeauty.beauty_android_client&referrer='
      url += "utm_source%3D#{utm.source}"
      url += "%26utm_medium%3D#{utm.medium}"
      url += "%26utm_campaign%3D#{utm.campaign}"
      url += "%26utm_term%3D#{utm.term}" if utm.term
      url += "%26utm_content%3D#{utm.content}" if utm.content
    else if app == 'shop'
      url = 'https://play.google.com/store/apps/details?id=kr.heybeauty.beauty_android_shop&referrer='
      url += "utm_source%3D#{utm.source}"
      url += "%26utm_medium%3D#{utm.medium}"
      url += "%26utm_campaign%3D#{utm.campaign}"
      url += "%26utm_term%3D#{utm.term}" if utm.term
      url += "%26utm_content%3D#{utm.content}" if utm.content

  else if platform == 'ios'
    if app == 'client'
      url = 'https://itunes.apple.com/kr/app/id1051173341?mt=8'
      url += "&pt=#{utm.token}"
      url += "&ct=#{ios_campaign_id}"
    else if app == 'shop'
      url = 'https://itunes.apple.com/kr/app/id1072621752?mt=8'
      url += "&pt=#{utm.token}"
      url += "&ct=#{ios_campaign_id}"

  else
    if app == 'client'
      url = link or 'http://heybeauty.me/'
    else if app == 'shop'
      url = link or 'http://heybeauty.me/shop/'

# redirect
hit = false
if url
  successCallback = -> if hit = !hit then location.href = url
  ga 'send', 'event', utm.source, utm.medium, utm.campaign, {hitCallback: successCallback}
  setTimeout successCallback, 1000
else
  alert '잘못된 URL 입니다'
  errorCallback = -> if hit = !hit then history.back()
  ga 'send', 'event', 'wrong_url', location.search, {hitCallback: errorCallback}
  setTimeout errorCallback, 1000